#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/adioz/Documents";
static const char *logpath = "/home/adioz/Wibu.log";
const char* animeku = "Animeku_";

void encode(char *str)
{
    int i;
    char newname[1000];
    for(i = 0; i < strlen(str); i++){
        char character = str[i];
        if(isupper(str[i])){
            character = 26 - (character - 'A') + 64;
            newname[i] = character;
        }else if(islower(str[i])){
            character = (character - 'a' + 13)%26 + 'a';
            newname[i] = character;
        }else{
            newname[i] = character; 
        }
    }

    strcpy(str, newname);

    return;
}

void renameFile(const char *path, const char *str){
    int i;
    char noExt[100] = "";
    char withExt[50] = "";
    int flag = 0;
    int idx = 0;
    for(i = 0; i < strlen(str); i++){
        if(str[i] == '.'){
            flag = 1;
            continue;
        }

        if(flag == 0){
            noExt[i] = str[i];
        }else{
            withExt[idx] = str[i];
            idx++;
        }
    }

    char oldname[1000] = "";
    strcat(oldname, path);
    strcat(oldname, "/");
    strcat(oldname, str);

    char newname[1000] = "";
    strcat(newname, path);
    strcat(newname, "/");
    encode(noExt);
    strcat(newname, noExt);
    //check if a file not a directory
    if(strstr(str, ".") != NULL){
        strcat(newname, ".");
        strcat(newname, withExt);
    }

    printf("newname: %s\n", newname);

    rename(oldname, newname);
}

int renameFileRecursively(const char* path)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char* cek = strstr(path, animeku);

    DIR *dp;
    struct dirent *de;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..")) continue;

        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(cek != NULL){
            if(S_ISDIR(st.st_mode)){
                char newpath[1000];
                sprintf(newpath, "%s/%s", path, de->d_name);
                renameFileRecursively(newpath);
            }

            renameFile(path, de->d_name);
        }

    }

    closedir(dp);

    return 0;
}

void writeLog(char* option, char* from, char* to){

    FILE* fp;
    fp = fopen(logpath, "a");

    if(fp == NULL){
        printf("ERROR\n");
        exit(1);
    }

    fprintf(fp, "RENAME %s %s -->\n%s\n", option, from, to);
    fclose(fp);
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);


    if (dp == NULL){
        printf("ERROR");
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        if(!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..")) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_rename(const char* from, const char* to)\
{
    int res;
    char fpath[1000], tpath[1000];

    if(strcmp(from, "/") == 0){
        from = dirpath;
        sprintf(fpath, "%s", from);
    }else{
        sprintf(fpath, "%s%s", dirpath, from);
    }

    if(strcmp(to, "/") == 0){
        to = dirpath;
        sprintf(tpath, "%s", to);
    }else{
        sprintf(tpath, "%s%s", dirpath, to);
    }


    res = rename(fpath, tpath);

    char* pathFrom = strstr(fpath, animeku);
    char* pathTo = strstr(tpath, animeku);


    if(pathFrom == NULL && pathTo != NULL){
        writeLog("terenkripsi", fpath, tpath);
        renameFileRecursively(tpath);
    }else if(pathFrom != NULL && pathTo == NULL){
        writeLog("terdecode", fpath, tpath);
        renameFileRecursively(tpath);
    }   

    if(res == -1) return -errno;

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_unlink(const char *path){
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = dirpath, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", dirpath, path);
    res = unlink(fpath);
    if (res == -1) return -errno;
    return 0;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .unlink = xmp_unlink
};



int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}