# Kelompok E09:
## Fayyadh Hafizh 5025201164
## M. Firman Fardiansyah
## Gery Febryan

### 1.

####    a. Pertama kita akan membuat fuse sederhana dengan beberapa operasi, yaitu get_attr, readdir, read, dan rename. 

####    b. Setelah itu kita akan membuat sebuah fungsi dengan nama encode yang berisi converter string menjadi atbash jika huruf besar dan rot13 jika huruf kecil.

        void encode(char *str)
        {
            int i;
            char newname[1000];
            for(i = 0; i < strlen(str); i++){
                char character = str[i];
                if(isupper(str[i])){
                    character = 26 - (character - 'A') + 64;
                    newname[i] = character;
                }else if(islower(str[i])){
                    character = (character - 'a' + 13)%26 + 'a';
                    newname[i] = character;
                }else{
                    newname[i] = character; 
                }
            }

            strcpy(str, newname);

            return;
        }

####    c. Dengan memanfaatkan fungsi encode, kita akan membuat fungsi renameFile yang akan memisahkan nama file dengan extensionnya dan melakukan encode pada nama filenya
        void renameFile(const char *path, const char *str)
        {
            int i;
            char noExt[100] = "";
            char withExt[50] = "";
            int flag = 0;
            int idx = 0;
            for(i = 0; i < strlen(str); i++){
                if(str[i] == '.'){
                    flag = 1;
                    continue;
                }

                if(flag == 0){
                    noExt[i] = str[i];
                }else{
                    withExt[idx] = str[i];
                    idx++;
                }
            }

            char oldname[1000] = "";
            strcat(oldname, path);
            strcat(oldname, "/");
            strcat(oldname, str);

            char newname[1000] = "";
            strcat(newname, path);
            strcat(newname, "/");
            encode(noExt);
            printf("%s\n", noExt);
            strcat(newname, noExt);
            //check if a file not a directory
            if(strstr(str, ".") != NULL){
                strcat(newname, ".");
                strcat(newname, withExt);
            }

            printf("%s %s\n", oldname, newname);

            rename(oldname, newname);
        }

####    d. Setelah itu kita akan menyiapkan fungsi renameFileRecursively untuk melakukan rename semua file yang ada didalam directory yang namanya mengandung kata "Animeku_" secara rekursif

        int renameFileRecursively(const char* path)
        {
            char fpath[1000];

            if(strcmp(path,"/") == 0)
            {
                path=dirpath;
                sprintf(fpath,"%s",path);
            } else sprintf(fpath, "%s%s",dirpath,path);

            char* cek = strstr(path, animeku);

            DIR *dp;
            struct dirent *de;

            dp = opendir(path);

            if (dp == NULL) return -errno;

            while ((de = readdir(dp)) != NULL) {
                if(!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..")) continue;

                struct stat st;

                memset(&st, 0, sizeof(st));

                st.st_ino = de->d_ino;
                st.st_mode = de->d_type << 12;

                printf("%s\n", cek);

                if(cek != NULL){
                    printf("%s\n", "MASUK");
                    if(!S_ISREG(de->d_type)){
                        char newpath[1000] = "";
                        sprintf(newpath, "%s/%s", path, de->d_name);
                        printf("newpath: %s\n", newpath);
                        renameFileRecursively(newpath);
                    }

                    printf("renaming file: %s/%s\n", path, de->d_name);
                    renameFile(path, de->d_name);
                }

            }

            closedir(dp);

            return 0;
        }


####    e. Untuk setiap perubahan nama directory akan dicatat dan dimasukkan ke dalam file Wibu.log

        void writeLog(char* option, char* from, char* to){

            FILE* fp;
            fp = fopen(logpath, "a");

            if(fp == NULL){
                printf("ERROR\n");
                exit(1);
            }

            fprintf(fp, "RENAME %s %s -->\n%s\n", option, from, to);
            fclose(fp);
        }

#### Untuk isi awal dari fuse, maka akan terlihat seperti ini

![image-2.png](./image-2.png)

#### Ketika semua folder diubah namanya menjadi "Animeku_..." maka akan terlihat seperti ini

![image-1.png](./image-1.png)

#### Ketika perubahan nama directory terjadi maka akan tercatat didalam Wibu.log

![image-3.png](./image-3.png)


#### f. Kendala
##### Kendala selama pengerjaan praktikum modul 4 kali ini adalah pada bug yang terjadi pada saat renaming nama yang memunculkan simbol-simbol yang tidak diketahui asalnya darimana

